#pragma once
module armarx
{
    interface MultiThreadingExampleServerInterface
    {
        void setValue(string val);
        string getValue();
    };
};
