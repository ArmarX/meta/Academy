/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MultiThreadingExample::ArmarXObjects::MultiThreadingExampleServer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MultiThreadingExampleServer.h"


using namespace armarx;


void MultiThreadingExampleServer::setValue(const std::string& value, const Ice::Current&)
{
    inputValue = value;
}

std::string MultiThreadingExampleServer::getValue(const Ice::Current&)
{
    return outputValue;
}

void MultiThreadingExampleServer::onInitComponent()
{

}


void MultiThreadingExampleServer::onConnectComponent()
{
    serverTask->start();
}


void MultiThreadingExampleServer::onDisconnectComponent()
{
    serverTask->stop();
}


void MultiThreadingExampleServer::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr MultiThreadingExampleServer::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new MultiThreadingExampleServerPropertyDefinitions(
            getConfigIdentifier()));
}

void MultiThreadingExampleServer::runningTask()
{
    while (!serverTask->isStopped())
    {
        std::string tmp;
        {
            std::lock_guard lock(inputMutex);
            tmp = inputValue; //keep lock time short
        }
        sleep(2); //sleep simulates long computation time
        std::hash<std::string> string_hash;
        ARMARX_IMPORTANT << "The hash value of \"" + inputValue + "\" is " + ValueToString(string_hash(inputValue));
        {
            std::lock_guard lock(outputMutex);
            outputValue = tmp; //keep lock time short
        }
    }
}
