set(SendingCustomTypesViaIce_INTERFACE_DEPEND ArmarXCore)

set(SLICE_FILES CustomTypeLooper.ice EigenTypeLooper.ice)
set(SLICE_FILES_ADDITIONAL_HEADERS CustomType.h)

# generate the interface library
armarx_interfaces_generate_library(SendingCustomTypesViaIce "${SendingCustomTypesViaIce_INTERFACE_DEPEND}")
