#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <ArmarXCore/interface/serialization/BoostFusionAdaptor.ice>
#include <ArmarXCore/interface/serialization/Variant.ice>
[["cpp:include:SendingCustomTypesViaIce/interface/CustomType.h"]] // we need to include it here

module armarx
{
    //the defined name has to be different than the type name
    ARMARX_MAKE_STRUCT_ADAPTOR_ICE(custom_struct_via_ice_1ada, ::armarx::custom_struct_via_ice_1);
    ARMARX_MAKE_STRUCT_ADAPTOR_ICE(custom_struct_via_ice_2ada, ::armarx::custom_struct_via_ice_2);
    ARMARX_MAKE_VARIANT_ICE(IntStrVar, ::boost::variant<int,std::string>);


    interface CustomTypeLooperInterface
    {
        void receive(
            ::Eigen::Vector3f q
            ,::Eigen::Matrix6f w
            ,custom_struct_via_ice_1ada c1 //here we have to use the defined name
            ,custom_struct_via_ice_2ada c2
            ,IntStrVar isv1
            ,IntStrVar isv2
                );
    };

};
