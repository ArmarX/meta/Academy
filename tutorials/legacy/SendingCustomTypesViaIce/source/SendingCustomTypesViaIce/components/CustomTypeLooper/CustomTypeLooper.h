/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SendingCustomTypesViaIce::ArmarXObjects::CustomTypeLooper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <SendingCustomTypesViaIce/interface/CustomTypeLooper.h>

namespace armarx
{
    /**
     * @defgroup Component-CustomTypeLooper CustomTypeLooper
     * @ingroup SendingCustomTypesViaIce-Components
     * A description of the component CustomTypeLooper.
     *
     * @class CustomTypeLooper
     * @ingroup Component-CustomTypeLooper
     * @brief Brief description of class CustomTypeLooper.
     *
     * Detailed description of class CustomTypeLooper.
     */
    class CustomTypeLooper :
        virtual public armarx::Component,
        virtual public CustomTypeLooperInterface
    {
    public:
        //here we can use the original type names or the defined ones
        void receive(const ::Eigen::Vector3f& inc,
                     const ::Eigen::Matrix6f& mx,
                     const ::armarx::custom_struct_via_ice_1& c1,
                     const ::armarx::custom_struct_via_ice_2ada& c2,
                     const ::boost::variant<int, std::string>& isv1,
                     const ::boost::variant<int, std::string>& isv2,
                     const Ice::Current& c = Ice::emptyCurrent) override;


        CustomTypeLooperInterfacePrx topic;


        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        std::string getDefaultName() const override
        {
            return "CustomTypeLooper";
        }
    };
}
