armarx_component_set_name("ExampleStateChartGroup")
set(COMPONENT_LIBS RobotAPIInterfaces ArmarXCoreStatechart ArmarXCoreObservers)

# Sources

set(SOURCES
ExampleStateChartGroupRemoteStateOfferer.cpp
./MainState.cpp
./CounterState.cpp
./MoveJoints.cpp
./SetJoints.cpp
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
)

set(HEADERS
ExampleStateChartGroupRemoteStateOfferer.h
ExampleStateChartGroup.scgxml
./MainState.h
./CounterState.h
./MoveJoints.h
./SetJoints.h
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
./MainState.xml
./CounterState.xml
./MoveJoints.xml
./SetJoints.xml
#@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.xml
)

armarx_add_component("${SOURCES}" "${HEADERS}")
