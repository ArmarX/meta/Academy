/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointPositionPassThroughController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_LIB_RobotUnitNJointControllerTutorial_NJointPositionPassThroughController_H
#define _ARMARX_LIB_RobotUnitNJointControllerTutorial_NJointPositionPassThroughController_H

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointPositionPassThroughController);
    TYPEDEF_PTRS_HANDLE(NJointPositionPassThroughControllerConfig);

    class NJointPositionPassThroughControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointPositionPassThroughControllerConfig(const std::string& name): deviceName {name} {}
        std::string deviceName;
    };

    /**
    * @ingroup RobotUnitNJointControllerTutorial
    * @brief  This controller provides functions for the gui and uses the target from the gui as setpoint.
    * It also uses the debug drawer to visualize something during its publish call
    */
    class NJointPositionPassThroughController: public NJointController
    {
    public:
        // ////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////// gui code ////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////// //
        static WidgetDescription::WidgetPtr GenerateConfigDescription
        (
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>& controlDevices,
            const std::map<std::string, ConstSensorDevicePtr>& sensorDevices
        );
        static NJointControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& value, const Ice::Current&) override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;


        // ////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////// setup code ///////////////////////////// //
        // ////////////////////////////////////////////////////////////////////// //
        NJointPositionPassThroughController(RobotUnitPtr prov, NJointControllerConfigPtr config, const VirtualRobot::RobotPtr&);
        std::string getClassName(const Ice::Current&) const override;

        // ////////////////////////////////////////////////////////////////////// //
        // //////////////////////////// control code //////////////////////////// //
        // ////////////////////////////////////////////////////////////////////// //
        void rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& /*timeSinceLastIteration*/) override;

        // ////////////////////////////////////////////////////////////////////// //
        // //////////////////////////// publish code //////////////////////////// //
        // ////////////////////////////////////////////////////////////////////// //
        void onPublishActivation(const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&) override;
        void onPublishDeactivation(const DebugDrawerInterfacePrx& drawer, const DebugObserverInterfacePrx&) override;
        void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx& drawer, const DebugObserverInterfacePrx&) override;

        // ////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////// data //////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////// //
        const SensorValue1DoFActuatorPosition* sensor;
        ControlTarget1DoFActuatorPosition* target;
        std::atomic<float> targetPos {0};
        std::atomic<float> currentPos {0};
    };
}
#endif
