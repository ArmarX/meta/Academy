/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointStacktraceController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <functional>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointStacktraceController);
    TYPEDEF_PTRS_HANDLE(NJointStacktraceControllerConfig);

    class NJointStacktraceControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointStacktraceControllerConfig(const std::string& name, const std::string& mode, float spamDelay)
            :
            deviceName {name},
            controlMode {mode},
            spamDelay {spamDelay}
        {}
        std::string deviceName;
        std::string controlMode;
        float spamDelay;
    };

    /**
    * @ingroup RobotUnitNJointControllerTutorial
    * @brief This controller prints a stacktrace in every call (used for testing).
    */
    class NJointStacktraceController: public NJointController
    {
    public:
        using ConfigPtrT = NJointStacktraceControllerConfigPtr;

        static WidgetDescription::WidgetPtr GenerateConfigDescription
        (
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>& controlDevices,
            const std::map<std::string, ConstSensorDevicePtr>& sensorDevices
        );
        static NJointStacktraceControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointStacktraceController(RobotUnitPtr prov, NJointStacktraceControllerConfigPtr cfg, const VirtualRobot::RobotPtr&);

        std::string getClassName(const Ice::Current&) const override;

        void rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& /*timeSinceLastIteration*/) override;

        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& s, const StringVariantBaseMap& v, const Ice::Current&) override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
        void onPublishActivation(const DebugDrawerInterfacePrx& dr, const DebugObserverInterfacePrx& ob) override;
        void onPublishDeactivation(const DebugDrawerInterfacePrx& dr, const DebugObserverInterfacePrx& ob) override;
        void onPublish(const SensorAndControl& sc, const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& obs) override;

        std::function<void()> run;
    protected:
        void onInitNJointController() override;
        void onConnectNJointController() override;
        void onDisconnectNJointController() override;
        void onExitNJointController() override;

        float spamDelay;
    };

}
