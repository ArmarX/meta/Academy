/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointThrowExceptionController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointThrowExceptionController.h"

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

using namespace armarx;


std::string NJointThrowExceptionController::getClassName(const Ice::Current&) const
{
    return "NJointThrowExceptionController";
}

void NJointThrowExceptionController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    NJointDebugBaseController::rtRun();
    if (triggered)
    {
        throw std::logic_error{"EXCEPTION TRIGGERED"};
    }
}

NJointControllerRegistration<NJointThrowExceptionController> registrationControllerNJointThrowExceptionController("NJointThrowExceptionController");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointThrowExceptionController);
