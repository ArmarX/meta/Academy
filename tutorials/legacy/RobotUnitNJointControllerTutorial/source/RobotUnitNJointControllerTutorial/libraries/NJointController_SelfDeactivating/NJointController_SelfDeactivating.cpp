/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointController_SelfDeactivating
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXGui/libraries/DefaultWidgetDescriptions/DefaultWidgetDescriptions.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "NJointController_SelfDeactivating.h"

using namespace armarx;

WidgetDescription::WidgetPtr NJointController_SelfDeactivating::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>& sensorDevices)
{
    using namespace armarx::WidgetDescription;
    FormLayoutPtr layout = new FormLayout;
    StringComboBoxPtr boxDevice = new StringComboBox;
    boxDevice->name = "name";
    boxDevice->options.reserve(controlDevices.size());
    for (const auto& pair : controlDevices)
    {
        boxDevice->options.emplace_back(pair.first);
    }
    layout->children.emplace_back(makeFormLayoutElement("ControlDevice", boxDevice));
    return layout;
}

NJointController_SelfDeactivating::NJointController_SelfDeactivating(RobotUnitPtr robotUnit, NJointControllerConfigPtr config, const VirtualRobot::RobotPtr&)
{
    ARMARX_CHECK_EXPRESSION(robotUnit);
    NJointController_SelfDeactivating_ConfigPtr cfg = NJointController_SelfDeactivating_ConfigPtr::dynamicCast(config);
    ARMARX_CHECK_EXPRESSION_W_HINT(cfg, "The provided config has the wrong type! The type is " << config->ice_id());
    auto ctrlTarg = useControlTarget(cfg->deviceName, ControlModes::Position1DoF);
    ARMARX_CHECK_NOT_NULL(ctrlTarg);
    ARMARX_CHECK_EXPRESSION(ctrlTarg->isA<ControlTarget1DoFActuatorPosition>());
    target = &(ctrlTarg->asA<ControlTarget1DoFActuatorPosition>()->position);
    ARMARX_CHECK_NOT_NULL(target);
}

NJointControllerRegistration<NJointController_SelfDeactivating> registrationControllerNJointController_SelfDeactivating("NJointController_SelfDeactivating");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointController_SelfDeactivating);
