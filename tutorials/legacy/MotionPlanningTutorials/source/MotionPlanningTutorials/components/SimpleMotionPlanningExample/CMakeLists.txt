armarx_component_set_name(SimpleMotionPlanningExample)
set(COMPONENT_LIBS
    RobotComponentsInterfaces
    MotionPlanning
    RobotAPICore)
set(SOURCES SimpleMotionPlanningExample.cpp)
set(HEADERS SimpleMotionPlanningExample.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
