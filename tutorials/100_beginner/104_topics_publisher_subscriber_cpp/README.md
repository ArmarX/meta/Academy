**Objective:** 
Learn how to create a server component with an ice interface 
and a client component communicating via remote procedure calls (RPC) in ArmarX.

**Previous Tutorials:** [Understand Distributed Systems](../102_distributed_systems)

**Next Tutorials:** [ToDo](../directory)

**Table of Contents:**

[[_TOC_]]


# Section A

Text

## Section A.1

```shell
armarx start
```

# Section B

```cpp
int a = 0;
```
