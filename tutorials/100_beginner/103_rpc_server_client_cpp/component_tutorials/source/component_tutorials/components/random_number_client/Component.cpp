/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    component_tutorials::ArmarXObjects::random_number_client
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>


namespace component_tutorials::components::random_number_client
{

    const std::string
    Component::defaultName = "random_number_client";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Minimal form, suits common standard setups.
        def->component(server);

        // Override default name and description:
        if (false)
        {
            def->component(server, "random_number_server",
                           "Ice object name of the random number server component.");
        }

        // Old pattern:
        if (false)
        {
            def->defineOptionalProperty("cmp.random_number_server", "random_number_server",
                                        "Ice object name of the random number server.");
        }

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Old pattern:
        if (false)
        {
            usingProxy(getProperty<std::string>("cmp.random_number_server").getValue());
        }
    }


    void
    Component::onConnectComponent()
    {
        // Old pattern:
        if (false)
        {
            server = getProxy<random_number_server::ComponentInterfacePrx>(
                        getProperty<std::string>("cmp.random_number_server").getValue());
        }

        ARMARX_INFO << "[Client] Send request to server.";

        int randomNumber = server->generateRandomNumber();

        ARMARX_INFO << "[Client] Received response: " << randomNumber;
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace component_tutorials::components::random_number_client
