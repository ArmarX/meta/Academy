armarx_add_aron_library(object_instance_aron
    ARON_FILES
        aron/FramedPose.xml
        aron/ObjectInstance.xml
)


armarx_add_library(object_instance
    SOURCES
        ObjectInstance.cpp
        ProducerConsumer.cpp
        aron_conversions.cpp
    HEADERS
        ObjectInstance.h
        ProducerConsumer.h
        aron_conversions.h
        forward_declarations.h
    DEPENDENCIES
        ArmarXCoreInterfaces
        ArmarXCore
        RobotAPICore
        ArViz
        aroncommon
)


# armarx_add_test(objectTest
#     TEST_FILES
#         test/objectTest.cpp
#     DEPENDENCIES
#         memory_tutorial::object
# )
