/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    memory_tutorial::ArmarXObjects::object
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"

#include <memory_tutorial/object_instance/ObjectInstance.h>
#include <memory_tutorial/object_instance/aron/ObjectInstance.aron.generated.h>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/armarx.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>


namespace memory_tutorial
{

    void object_instance::toAron(arondto::FramedPose&dto, const FramedPose &bo)
    {
        dto.pose = bo.pose;
        dto.frame = bo.frame;
        dto.agent = bo.agent;
    }


    void object_instance::fromAron(const arondto::FramedPose& dto, FramedPose& bo)
    {
        bo.pose = dto.pose;
        bo.frame = dto.frame;
        bo.agent = dto.agent;
    }


    void object_instance::toAron(arondto::ObjectInstance &dto, const ObjectInstance &bo)
    {
        dto.name = bo.name;
        toAron(dto.pose, bo.pose);
        toAron(dto.fileLocation, bo.fileLocation);
        toAron(dto.oobb, bo.localOobb);
    }


    void object_instance::fromAron(const arondto::ObjectInstance &dto, ObjectInstance &bo)
    {
        bo.name = dto.name;
        fromAron(dto.pose, bo.pose);
        fromAron(dto.fileLocation, bo.fileLocation);
        fromAron(dto.oobb, bo.localOobb);
    }

}
