#pragma once

#include <optional>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include <memory_tutorial/object_instance/forward_declarations.h>


namespace memory_tutorial::object_instance
{

    class ProducerConsumer
    {
    public:

        object_instance::ObjectInstance produce();

        void consume(const object_instance::ObjectInstance& instance);


    public:

        std::optional<armarx::viz::Client> arviz;

    };

}

