# Viewing Camera Recordings Created Using an ImageRecorder from ArmarX

[[_TOC_]]

This is a how-to on viewing camera recordings created by ArmarX image recording
plugins, similar to what is described [here](../record_multiple_image_streams_simultaneously).


To view recordings made using Azure Kinect cameras present on ArmarDE or Armar6,
**VisionX** needs to be compiled / installed using axii.

---
The folder structure created by Azure Kinect cameras looks something like this:
```
├── OpenNIPointCloudProvider_depth
│   ├── metadata.csv
│   ├── chunk_0
│   ├── chunk_1
│   └── ...
└── OpenNIPointCloudProvider_rgb
    ├── metadata.csv
    ├── chunk_0
    ├── chunk_1
    └── ...
```
The folders named `chunk_xxx` contain the actual images.

In ArmarX, the Application `PlaybackImageProviderApp` enables viewing of image sequences.
Create a new scenario in the `ScenarioManager`. To this scenario, add three applications:
- PlaybackImageProviderApp
- RemoteGuiProviderApp
- MemoryNameSystem

By clicking on the PlaybackImageProviderApp, we can edit its config. Modify the
parameter `ArmarX.PlaybackImageProvider.recording_files` to point to the folder
containing the images, or to the folder containing `metadata.csv`. Specifying multiple
paths in a semicolon-separated list allows side-by-side viewing of multiple recordings.

Now that the configuration of the scenario is complete, we can add the widget that
will actually allow us to view the images: `Add Widget`->`VisionX`->`ImageMonitor`.

In the `ImageMonitor` widget, we need to specify the image source. Click on the wrench
icon in the lower left of the application window. In the popup, specify
`PlaybackImageProvider` as the proxy.

In the settings, we can also handle depth images by specifying the correct channel and
specifying a maximum distance for visualization. However, handling depth images can come
at the cost of some performance.
