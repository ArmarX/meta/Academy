[[_TOC_]]


# How to Interactively Edit the Object Memory

## Requirements

The ObjectMemoryEditor allows to modify scenes stored in the object memory.
To begin editing, you need the ArMemCore scenario and any scenario that provides an ObjectMemory and ObjectPoseProvider.
For the examples here, the ArMemObjectMemory scenario is used.

The actual editor is a component too, which is part of the edit_object_memory scenario.
After starting all the components, you only need ArViz Godot to see and interact with the scene.
ArViz Godot can be started using the icon in the ArmarX GUI or with the ```arviz-godot``` command.
Using the normal ArViz visualization client is possible too, although the controls differ. 
For help on using ArViz Godot itself, use the *Scene* > *Help* menu in the upper left corner.

## How to Edit

ArViz Godot will show all current layers, but you only want the layers of the ObjectMemoryEditor.
Use the layer menu in the top right corner to disable all other layers.

Now you can begin editing all objects in the scene.
Click on an object to select it, which will show a simple selection indicator.
In the upper left corner of the screen, you activate either the *Move* or *Rotate* mode and then use the gizmos to transform a selected object.

With a left mouse click on an object the context menu is opened.
The first set of options work affect only the current object:
* *Clone* creates a new object of the same type as the current object.
* *Delete* marks the current object for deletion.
* *Reset* resets all changes of the current object. If the current object is a clone, it will be removed.
* *Create Placeholder* creates a placeholder object, which can then be replaced with a specific object.

There are two ways of adding new objects to the scene.
Cloning is simple and direct, but the new object of same type as the original.
To create objects of different type placeholders are used.
Placeholders can be moved around the scene like real object, but they are not actually part of the scene.
By left-clicking on them a list with all available object types is opened.
Selecting one of the entries replaced the placeholder with a new object of selected type.

Changed elements in the scene are drawn transparently, their color indicating the kind of the change.
These changes are only stored locally and are not yet committed to the memory.
To manage all changes, the second set of options on each object is used.
* *Commit All Changes* will commit the changes, which will cause all transparent objects to become opaque again. Note that the ObjectMemory might now show a new layer that has to be disabled again.
* *Update Unchanged* updates the stored state, without affecting the changes. This option is necessary because the editor does not continuously update its state.
* *Reset All* resets all changes, restoring the state of the last update.

By default, all objects in the scene are scaled up by 1% to simplify selecting them.
This can be adjusted in the options of the component.

## Saving and Loading a Scene

After creating a scene, the next step is to save it.
This is done using the RemoteGui of the ObjectMemory.
Open the RemoteGui in ArmarX GUI.
In the *Data* box, type in a name for the current scene and press *Store Scene*.

To load a scene, set the correct name of the scene in the SnapshotToLoad option of the ObjectMemory.
 