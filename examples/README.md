# ArmarX Examples

This is a collection of links to example components.
In contrast to step-by-step tutorials, these have been made
as references for how to do a specific thing or a specific API.

To use an example, look at the code and try to port it to your own project.
Feel free to **copy, paste and adapt** the code to your needs, 
but **do not** use the component or its code directly in your project (e.g. as a library). 

Example usages of the ArmarX Python bindings can be found 
[here](https://gitlab.com/ArmarX/python3-armarx/-/tree/master/examples).


[[_TOC_]]


### Components / Ice 

- [**rng_proxy.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/rng_proxy.py):
  How to implement a component, i.e. implement an Ice interface and offer it in Ice, in Python.


### Remote GUI

- [**Remote GUI Example (C++)**](https://gitlab.com/ArmarX/ArmarXGui/-/blob/master/source/ArmarXGui/applications/RemoteGuiExample/RemoteGuiExample2.h):
  How to create a simple GUI for changing parameters or triggering events.
  Part of the scenario **RemoteGuiTest** in ArmarXGui.
- [**remote_gui.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/remote_gui.py): 
  How to use the Python client API of the Remote GUI.


### Visualization (ArViz)

- [**Arviz Example (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/ArViz/Example): 
  How to use the C++ client API of the ArmarX 3D visualization framework ArViz. 
  Part of the scenario **ArVizExample** in RobotAPI.
- [**ArViz Interact Example (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/blob/master/source/RobotAPI/components/ArViz/Example/ArVizInteractExample.cpp)
  How to use the **interactive** features of ArViz.
- [**arviz.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/arviz.py):
  How to use Python client API of ArViz.
- [**arviz_interact.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/arviz_interact.py):
  How to use the interactive features of ArViz from Python.
- [**arviz_sphere_mesh.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/arviz_sphere_mesh.py):
  How to draw custom meshes in ArViz from Python.


### Robot State

- [**VirtualRobotReaderExampleClient (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/armem/client/VirtualRobotReaderExampleClient):
  How to read a robot's state from the Robot State Memory using the `VirtualRobotReader`.
- [**VirtualRobotWriterExample (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/armem/client/VirtualRobotWriterExample):
  How to write a robot's state to the Robot State Memory using the `VirtualRobotWriter`.


### Vision

- [**publish_images.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/publish_images.py):
  How to publish images from Python.
- [**process_images.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/process_images.py):
  How to receive and process images in Python.
- [**publish_point_clouds.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/publish_point_clouds.py):
  How to publish point clouds from Python.
- [**process_point_clouds.py (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/process_point_clouds.py):
  How to receive and process point clouds in Python.


### Memory System (General)

- [**Example Memory (Server) (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/armem/server/ExampleMemory):
  Example memory server.
- [**Example Memory Client (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/armem/client/ExampleMemoryClient):
  Example memory client accessing data from the example memory server. 
- [**Example Memory Client (Python)**](https://gitlab.com/ArmarX/python3-armarx/-/blob/master/examples/memory_client.py):
  Example memory client accessing data from the example memory server in Python.

Try them in the scenario **ArMemExample** in RobotAPI.


### Object Memory / Instances / Poses

- [**ObjectPoseClientExample (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/ObjectPoseClientExample): 
  Getting the current object instances from the robot's Object Memory.

- [**ObjectPoseProviderExample (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/blob/master/source/RobotAPI/components/ObjectPoseProviderExample): 
  Providing localized object observations / object instances to the Object Memory.    


### Skill Frameworks 

- [**SkillProviderExample (C++)**](https://gitlab.com/ArmarX/RobotAPI/-/tree/master/source/RobotAPI/components/skills/SkillProviderExample):
  How to implement a skill and make it available in the skill framework.

- [**Navigation example_client (C++)**](https://gitlab.com/ArmarX/skills/navigation/-/tree/master/examples/components/example_client):
  How to use the navigation skill / framework. 
